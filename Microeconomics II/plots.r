#Plot graph in line 275
pdf(file="fig1_1.pdf")
plot(NULL,xlim=c(0,3),ylim=c(0,3),bty="n",xlab="Cost",ylab="Quantity")
lines(c(0,3),c(2.5,1),col="blue",lwd=2)
lines(c(0,3),c(2,0.5),col="blue",lwd=2)
lines(c(3,3),c(0,1),col="black",lwd=1,lty=2)
text(1.5,2,"Firm 1")
text(1,1.3,"Firm 2")
dev.off()

#Plot graph in line 328
pdf(file="fig1_2.pdf")
plot(NULL,xlim=c(0,1),ylim=c(0,1),bty="n",xlab="Cost",ylab="Quantity",axes=F)
axis(2,at=c(0,0.5,1),labels=c(0,"p","d"))
axis(1,at=c(0,0.5,1),labels=c(0,"q","d"))
lines(c(0,1),c(1,0))
lines(c(0,0.5),c(0.5,0.5),lty=2)
lines(c(0.5,0.5),c(0,0.5),lty=2)
text(0.1,0.7,expression(S(q) == frac(q^2,2)))
dev.off()

#Plot for line 472
a<-runif(1000,1,2)
b<-punif(a,1,2)

pdf(file="fig1_3.pdf")
plot(a,b,type="l",bty="n",xlim=c(0,3),ylim=c(0,1),xlab="Cost",ylab="CDF",axes=F)
axis(1,at=c(0,1,1.5,2,3),labels=c(0,1,expression(hat(c)[j]),2,3))
axis(2,at=0:1)
lines(c(1,0),c(0,0))
lines(c(2,3),c(1,1))
lines(c(1.5,1.5),c(0,0.5),col="black",lwd=1,lty=2)
dev.off()

#tomega
y1<-function(x) 2*(x-2)^3 + 10
x1<-seq(0,10,0.01)

y2<- function(x) 2*(x-2)^3 + 15
x2<-seq(0,2,0.01)
x3<-seq(2,10,0.01)

pdf(file="tomega.pdf",width=7, height=4)
par(xaxs="i",yaxs="i",mfrow=c(1,2))

plot(x1,y1(x1),type="l",ylim=c(0,20),xlim=c(0,4),axes=F,xlab=expression(theta),ylab="t")
axis(1,labels=F,lwd.tick=0)
axis(2,at=c(0,10,20),labels=c("",expression(hat(t)),""),lwd.tick=0)
lines(c(0,2),c(10,10),lty=2)

plot(x2,y1(x2),type="l",ylim=c(0,20),xlim=c(0,4),axes=F,xlab=expression(theta),ylab="t")
lines(x3,y2(x3))
axis(1,labels=F,lwd.tick=0)
axis(2,labels=F,lwd.tick=0)
lines(c(2,2),c(10,15),lty=2)
dev.off()