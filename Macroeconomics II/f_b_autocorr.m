function F=f_b_autocorr(x,y,k)

%x is fixed variable


R = zeros(2*k+1,1);
j=1;
s = length(y);
for i=-k:0
    y_t = y(1:s+i);
    x_t = x(-i+1:s);
    R(j,1)= corr(x_t,y_t);
    j = j+1;
end

for i=1:k
    y_t = y(1+i:s);
    x_t = x(1:s-i);
    R(j,1)= corr(x_t,y_t);
    j=j+1;
end


F=R;