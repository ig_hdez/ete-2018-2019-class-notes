\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{anysize}
\usepackage{cancel}
\usepackage{xcolor}
%\usepackage{enumerate}
\usepackage{amsmath, amsthm, amsfonts,amssymb}
\usepackage{graphicx}
\marginsize{2cm}{2cm}{2cm}{2cm}
\renewcommand{\baselinestretch}{1.5}

\title{Asset Pricing}
\author{José Ignacio Hernández}
\date{\today}

\begin{document}

\maketitle

\section{Asset Pricing (from Macro I)}

\subsection{Lucas Asset Pricing Model}

We will analyze first the Lucas Asset Pricing Model. Suppose an endowment economy with one agent who must consume a good $c$. This individual perceives an utility $u(c_t)$ for each period $t$. There exists an stochastic Markov Chain process $z^t$ which gives an uncertainty framework to the problem.

\underline{Household's problem:} the individual solves:

\begin{align*}
\max_{c_t(z^t)}	& \sum_t \sum_{z^t} \beta^t \pi(z^t) u\left(c_t(z^t) \right)	\\
\text{s.t. }	& \sum_t \sum_{z^t} p_t(z^t)c_t(z^t) = \sum_t \sum_{z^t} p_t(z^t)\bar{\omega}_t(z^t)\\
				& c_t(z^t) = \bar{\omega}_t (z^t), \forall t, \forall z^t
\end{align*}

Notice that this problem is not different than a classic general equilibrium problem: the representative agent chooses a consumption bundle that maximizes their expected utility, subject to a budget constraint and a market clearing condition. Since the last constraint is also the feasibility constraint, the allocation problem is trivial, and only prices $p_t(z^t)$ supporting this allocation as an equilibrium must be sought.

Now, we need to find an expression for $p_t(z^t)$ in model parameters. The Lagrangian is:

\begin{equation}
\mathcal{L} = \sum_t \sum_{z^t} \beta^t \pi(z^t) u\left(c_t(z^t) \right)
			+ \lambda \left[ \sum_t \sum_{z^t} \left( p_t(z^t)\bar{\omega}_t(z^t) - p_t(z^t)c_t(z^t) \right) \right]
\nonumber
\end{equation}

Solving for $c_t$:

\begin{equation}
\beta^t \pi(z^t) u'\left(c_t(z^t) \right) = \lambda p_t(z^t)
\nonumber
\end{equation}

And $c_t (z^t) = \bar{\omega}_t(z^t)$ for all $t$.

\textbf{Interpretation:} Marginal benefit of consumption equal to marginal cost.

Notice that for $t=0$, $\pi(z^0) = 1$ (we know with certainty what happens in the beginning) and $c_0 = \bar{\omega}_0
$, given that we start with the initial endowment with certainty. Then normalizing $p_0 = 1$:
\begin{equation}
u'\left(\bar{\omega}_0 \right) = \lambda 
\nonumber
\end{equation}

Plugging this expression in the FOC of $c_t$:

\begin{align*}
\beta^t \pi(z^t) u'\left(c_t(z^t) \right) & = \left( u'\left(\bar{\omega}_0 \right) \right) p_t(z^t)	\\
p_t(z^t) & = \beta^t \pi(z^t) \frac{u'\left(\bar{\omega}_t(z^t) \right)}{u'\left(\bar{\omega}_0 \right)}
\end{align*}

This is the \textbf{Lucas Pricing Formula}: $p_t(z^t)$ is the price of a claim on consumption goods at $t$ that yields 1 if $z^t$ realizes, and zero otherwise.

\begin{itemize}
	\item $p_t(z^t)$ is decreasing in time, given the discount factor.
	\item $p_t(z^t)$ is increasing in the probability of realization of state $z^t$.
	\item $p_t(z^t)$ depends of the intertemporal MRS between $t$ and the state$z^t$, and $t=0$.
\end{itemize}

Now we should want to apply this formula to price arbitrary assets. Any asset is nothing than a sum of contingent claims, so pricing an asset consists of summing up the prices of these rights.

\subsubsection{Risk-Free Asset}

We must find the price at $t$ and $z^t$ of an asset that pays 1 at $t+1$ for every possible $z^{t+1}$, such that:

\begin{equation}
z^{t+1} = (z_{t+1},z^t)
\nonumber
\end{equation}

For $z_{t+1} \in Z$. Notice that this is the definition of a \textbf{risk-free asset}.

The price of one unit of a risk-free asset at $t=0$ is:

\begin{equation}
q_0^{rf} (z^t) = \sum_{z' \in Z} p_{t+1}(z',z^t) \times 1
\nonumber
\end{equation}

In period $t$:

\begin{align*}
q_t^{rf} (z^t) 	& = \frac{q_0^{rf}}{p_t(z^t)}	\\
& = \frac{\sum_{z' \in Z} p_{t+1}(z',z^t)}{p_t(z^t)}	\\
& = \frac{\sum_{z' \in Z} \beta^{t+1} \pi(z',z^t) \frac{u'\left(\bar{\omega}_{t+1}(z',z^t) \right)}
{u'\left(\bar{\omega}_0 \right)}}
{\beta^t \pi(z^t) \frac{u'\left(\bar{\omega}_t(z^t) \right)}{u'\left(\bar{\omega}_0 \right)}}	\\
& = \beta \sum_{z' \in Z} \frac{\pi(z',z^t)}{\pi(z^t)} \frac{u'\left(\bar{\omega}_{t+1}(z',z^t) \right)}{u'\left(\bar{\omega}_t(z^t) \right)}
\end{align*}

\textbf{Interpretation:} The price of an asset is given by the discounted (respect to the following period) conditional expected value with probability state $z'$ in $t+1$, given the occurrence of $z^t$ in $t$ of the intertemporal MRS of consumption between $t+1$ and $t$.

\subsubsection{Stock that Pays Dividends}

Suppose that we have a stock in period $t$ that pays dividends $d_t(z^t)$ from $t+1$ permanently. The price of this asset is:

\begin{align*}
q_t^{tree} (z^t) & = \frac{\sum_{s=t+1}^\infty \sum_{z^s} p_s (z^s) d_s(z^s)}{p_t(z^t)}	\\
& = E_t \left( \sum_{s=t+1}^\infty \beta^{s-t} \frac{u'(\bar{\omega}_s)}{u'(\bar{\omega}_t(z^t))} d_s \right)
\end{align*}

We can also write this expression in a recursive way. Notice that the price of an asset could be reflected as:

\begin{equation}
p_t = \frac{p_{t+1} + d_{t+1}}{R_{t+1}}
\nonumber
\end{equation}

Where $R_{t+1}$ is the interest rate between $t$ and $t+1$. In stochastic way:

\begin{equation}
q_t^{tree} = \beta \sum_{z^t} \beta \frac{\pi(z',z^t)}{\pi(z^t)} \frac{u'(\bar{\omega_{t+1}}(z',z^t))}{u'(\bar{\omega_{t}}(z^t)))} [d_{t+1}+q_{t+1}^{tree}]
\nonumber
\end{equation}

Or,

\begin{equation}
q_t^{tree} = \beta E_t \left[ \frac{u'(\bar{\omega_{t+1}}(z',z^t))}{u'(\bar{\omega_{t}}(z^t)))} [d_{t+1}+q_{t+1}^{tree}]
\right]
\nonumber
\end{equation}

$q_{t+1}^{tree}(z',z^t)$ will have to be such that at date $t+1$ and state $(z',z^t)$, the consumer is indifferent between purchasing the tree again, or using the proceeds to consume goods. The expression:

\begin{equation}
\beta E_t \left[ \frac{u'(\bar{\omega_{t+1}}(z',z^t))}{u'(\bar{\omega_{t}}(z^t)))} \right]
\nonumber
\end{equation}

is called the \textbf{Stochastic Discount Factor} or \textbf{Pricing Kernel}.

\subsection{The Equity Premium Puzzle}

The equity premium is defined as the difference between the returns on stocks (risky) and government bonds (risk-free). But, what we see on data? We detail the average US Stock Market returns from 1871 to 2009 and compare with Risk-Free rate, taken from US government bonds. The difference in returns from these two assets is equal to the \textbf{equity premium}.

\begin{table}[ht]
\centering
\begin{tabular}{lrr}
Item					&	Mean			&	Std. Dev.	\\
\hline\hline
Stock Returns			&	8\%				&	17.88\%		\\
Risk-Free Rate			&	2.82\%			&	6.58\%		\\
\hline
\textbf{Equity Premium}	& \textbf{5.18\%}	& \textbf{18.32\%}
\end{tabular}
\end{table}

Indeed, investors who have their shares in risky assets should have (in the long run) a return 5\% higher than if they were put their money in US bonds. Since stocks are risker than bonds, that should be explained by the representative's agent ``dislike for risk'', which in the CES utility function is captured by the parameter $\sigma$.

Mehra and Prescott use the Lucas model in order to test if it account for the equity premium observation in the data. The endowment process is characterized by two parameters to match with observed data.

\subsubsection{Model}

Preferences are modeled by:

\begin{equation}
U = E_0 \left[ \sum_{t=0}^\infty \beta^t u(c_t) \right]
\nonumber
\end{equation}

Where:

\begin{equation}
u(c) = \frac{c^{1-\sigma}-1}{1-\sigma}
\nonumber
\end{equation}

The key parameters are $\beta$, which measures time impatience, and $\sigma$ measures both intertemporal substitution and relative risk aversion.

The endowment process is described as:

\begin{equation}
y_{t+1} = x_{t+1}y_t
\nonumber
\end{equation}

Where the growth rate $x_{t+1}$ is a random variable who follows a \textbf{Markov chain}, where:

\begin{equation}
\phi_{ij} = P[x_{t+1}=\lambda_j|x_t=\lambda_i
\nonumber
\end{equation}

To obtain the asset prices, we use the Lucas price formula to the asset that yields $d_t = y_t$ at time $t$:

\begin{equation}
p_t^e = E_t \left[ \sum_{s=t+1}^\infty \beta^{s-t} \frac{u'(y_s)}{u'(y_t)} d_s \right]
\nonumber
\end{equation}

$p_t^e$ is the price of equity, the ``market portfolio'', which is the value of a portfolio of claims on all possible productive investments. The nearest proxy for this portfolio is the stock market. 

\subsubsection{Solving the Model}

We will solve the price recursively. Notice that, given the 1st. order Markov assumption on $x_t$, the probability of changing states does not vary through time. So:

\begin{equation}
p_t^e = p_e(x_t,y_t)
\nonumber
\end{equation}

All the information about market prices are given by the endowment process $y_t$ and the last realization of $x_t$. Since in equilibrium consumption equals endowment, $y_t$ provide the level of marginal utility against which future consumption streams will be compared when setting prices, and $x_t$ conveys information of the part of the Markov process where the economy is standing. Only $x_t$ is relevant, not the lagged values.

Then, the recursive formulation of equity prices is:

\begin{equation}
p_e(x_t,y_t) = E \left[ \sum_{s=t+1}^\infty \beta^{s-t} \left( \frac{y_t}{y_s} \right)^\sigma y_s | x_t,y_t \right]
\nonumber
\end{equation}

For each state $x_i$, with $i=1,\ldots,n$ the price at any date $t$ is given by:

\begin{equation}
p_i^e(y) = \beta \sum_{j=1}^n \phi_{ij} \left( \frac{y}{y \lambda_j} \right) [y \lambda_j + p_j^e(y\lambda_j)]
,\forall y, \forall i
\nonumber
\end{equation}

Where $p_j^e(y\lambda_j)$ is the price of equity next period if the state is $j$. So, consumption (and endowment) growth will be $x_{t+1} = \lambda_j$.

We guess a linear solution to this equation:

\begin{equation}
p_i^e(y) = p_i^e y
\nonumber
\end{equation}

Which yields to the system of equations:

\begin{align*}
p_i^e 	& = \beta \sum_{j=1}^n \phi_{ij} \left( \lambda_j \right)^{-\sigma} [\lambda_j + p_j^e \lambda_j]	\\
		& = \beta \sum_{j=1}^n \phi_{ij} \left( \lambda_j \right)^{1-\sigma} [1 + p_j^e]
\end{align*}

For each $i=1,\ldots,n$. $p_i^e$ is the weighted sum of future prices. Notice that we have $n$ equations and $n$ unknowns, so the system is identified.

For the risk-free asset which pays 1 unit in every state:

\begin{equation}
p_i^{rf} (y) = \beta \sum_{j=1}^n \phi_{ij}x_j^{-\sigma}\cdot 1
\nonumber
\end{equation}

where $x_j$ is the ratio of consumption of next period respect present, i.e. consumption growth. This prices does not depend of endowments.

To compute the return realized at state $j$ by an investor who purchased the equity at state $i$ we use:

\begin{equation}
r_{ij}^e = \frac{(1+p_j^e)\lambda_j}{p^e_i} - 1
\nonumber
\end{equation}

Therefore, the conditional expected return is:

\begin{equation}
r^e_i = E_i[r^e_{ij}] = \sum_{j=1}^n \phi_{ij} r^e_{ij}
\nonumber
\end{equation}

While the unconditional expected return is:

\begin{align*}
r^e & = e[r^e_i] \\
r^e	& = \sum_{i=1}^n \pi_i r^e_i \\
r^e	& = \sum_{i=1}^n \pi_i \sum_{j=1}^n \phi_{ij} r^e_{ij}
\end{align*}

where $\pi_i$ is the unconditional (long run) probability of the Markov Process.

$r^e$ will be compared with the return on equity. The return of risk-free assets is computed as:

\begin{equation}
r_i^{rf} = \frac{1}{p_i^{rf}} - 1
\nonumber
\end{equation}

Which is a random variable. The long run return is given by:

\begin{equation}
r^{rf} = \sum_{i=1}^n \pi_i r^{rf}_i
\nonumber
\end{equation}

So, the equity premium is computed as the difference between $r^e$ and $r^{rf}$, which are compared with the return of equity and US government bond respectively.

\subsubsection{Calibration}

Mehra and Prescott calibrated their model assuming a $n=2$ state Markov process. So, each possible realization of the endowment growth rate is described by:

\begin{align*}
\lambda_1 & = 1 + \mu + \delta	\\
\lambda_1 & = 1 + \mu - \delta
\end{align*}

Where $\mu$ is the average growth rate of endowment (and consumption):

\begin{equation}
\mu = \frac{c_{t+1} - c_t}{c_t}
\nonumber
\end{equation}

And $\delta$ is the variation of the growth rate. A good approximation of $\delta$ could be the standard deviation of $\mu$.

The transition matrix is asssumed to be symmetric:

\begin{equation}
\Phi =
\begin{bmatrix}
\phi	& 1-\phi	\\
1-\phi	& \phi		\\
\end{bmatrix}
\nonumber
\end{equation}

To compute $\phi$, we take the 1st. order serial correlation of $\mu$ from an AR(1).

We will take our aggregate consumption data from 1889 to 2009 to calibrate the model. We also assume a value for $\beta = e^{-0.05}$ and $\sigma = 2$, in line with other studies. The results of both the data and calibrated model are:

\begin{table}[ht]
\centering
\begin{tabular}{lrr}
						&	\textbf{Data}			&	\textbf{Model}			\\
\hline
Item					&	Mean (Std.Dev)			&	Mean (Std.Dev)			\\
\hline\hline
Stock Returns			&	8\%	(17.88\%)			&	9.43\% (4.08\%)			\\
Risk-Free Rate			&	2.82\% (6.58\%)			&	9.15\% (0.57\%)			\\
\hline
\textbf{Equity Premium}	& \textbf{5.18\% (18.32\%)}	&	\textbf{0.28\% (4.65\%)}
\end{tabular}
\end{table}

We can observe some issues:

\begin{itemize}
	\item Stock returns are too smooth: the difference in Std. Deviation is too high.
	\item Risk-free returns are too high: the difference in Mean is too high.
	\item \textbf{The model does not fit the data in terms of the equity premium.} It predicts too low difference in returns.
\end{itemize}

The baseline Lucas tree model used by Mehra and Prescott only accounts for the data if we calibrate with very high risk-free interest rates (which does not make sense), or unreasonable time impatience rates (like $\beta>1$, which implies that individuals are too patient).
\end{document}