\babel@toc {english}{}
\contentsline {chapter}{\numberline {1}Asset Pricing}{9}{chapter.1}% 
\contentsline {section}{\numberline {1.1}Lucas Asset Pricing Model}{9}{section.1.1}% 
\contentsline {subsection}{\numberline {1.1.1}Risk-Free Asset}{10}{subsection.1.1.1}% 
\contentsline {subsection}{\numberline {1.1.2}A Stock that Pays Dividends}{11}{subsection.1.1.2}% 
\contentsline {section}{\numberline {1.2}The Equity Premium Puzzle}{11}{section.1.2}% 
\contentsline {subsection}{\numberline {1.2.1}Model}{12}{subsection.1.2.1}% 
\contentsline {subsection}{\numberline {1.2.2}Solving the Model}{12}{subsection.1.2.2}% 
\contentsline {subsection}{\numberline {1.2.3}Calibration}{14}{subsection.1.2.3}% 
\contentsline {chapter}{\numberline {2}Monetary Theory and Policy}{15}{chapter.2}% 
\contentsline {section}{\numberline {2.1}Introduction}{15}{section.2.1}% 
\contentsline {section}{\numberline {2.2}Money in the utility function}{17}{section.2.2}% 
\contentsline {section}{\numberline {2.3}Welfare Cost of Inflation}{21}{section.2.3}% 
\contentsline {section}{\numberline {2.4}The New Keynesian Model}{24}{section.2.4}% 
\contentsline {section}{\numberline {2.5}Monopolistic Competition (Dixit and Stiglitz)}{26}{section.2.5}% 
\contentsline {subsection}{\numberline {2.5.1}Model}{26}{subsection.2.5.1}% 
\contentsline {subsection}{\numberline {2.5.2}Benchmark Model}{28}{subsection.2.5.2}% 
\contentsline {section}{\numberline {2.6}Sticky Prices Within Period}{29}{section.2.6}% 
\contentsline {section}{\numberline {2.7}Dynamics}{30}{section.2.7}% 
\contentsline {subsection}{\numberline {2.7.1}Flexible Prices}{30}{subsection.2.7.1}% 
\contentsline {subsection}{\numberline {2.7.2}Sticky Prices}{31}{subsection.2.7.2}% 
\contentsline {subsection}{\numberline {2.7.3}Staggered price adjustement}{32}{subsection.2.7.3}% 
\contentsline {section}{\numberline {2.8}New Keynesian Phillips Curve (NKPC)}{36}{section.2.8}% 
\contentsline {section}{\numberline {2.9}Adjustment Dynamics}{36}{section.2.9}% 
\contentsline {section}{\numberline {2.10}Complementarities in price adjustment}{38}{section.2.10}% 
\contentsline {subsection}{\numberline {2.10.1}Primitive Forces for Complementarity}{39}{subsection.2.10.1}% 
\contentsline {subsection}{\numberline {2.10.2}Forces for Sustitutability}{39}{subsection.2.10.2}% 
\contentsline {section}{\numberline {2.11}New Keynesian Model}{39}{section.2.11}% 
\contentsline {chapter}{\numberline {3}Money Empirics}{41}{chapter.3}% 
\contentsline {section}{\numberline {3.1}Money in the Long Run}{41}{section.3.1}% 
\contentsline {section}{\numberline {3.2}Methodology}{41}{section.3.2}% 
\contentsline {section}{\numberline {3.3}Role of Money in the Business Cycle}{43}{section.3.3}% 
\contentsline {subsection}{\numberline {3.3.1}Correlation Analysis}{43}{subsection.3.3.1}% 
\contentsline {subsection}{\numberline {3.3.2}Econometric Models}{44}{subsection.3.3.2}% 
\contentsline {section}{\numberline {3.4}The VAR Approach of Money}{44}{section.3.4}% 
\contentsline {subsection}{\numberline {3.4.1}Example: a VAR on Product and Money}{45}{subsection.3.4.1}% 
\contentsline {section}{\numberline {3.5}The Recursive Assumption}{45}{section.3.5}% 
\contentsline {section}{\numberline {3.6}VAR issues}{47}{section.3.6}% 
\contentsline {section}{\numberline {3.7}Back to the New Keynesian Model}{47}{section.3.7}% 
\contentsline {section}{\numberline {3.8}Evaluating the NKPC}{47}{section.3.8}% 
\contentsline {section}{\numberline {3.9}The Hybrid NKPC}{49}{section.3.9}% 
\contentsline {section}{\numberline {3.10}Analysis}{49}{section.3.10}% 
\contentsline {chapter}{\numberline {4}Equilibrium Stability, Determinacy and Policy}{51}{chapter.4}% 
\contentsline {section}{\numberline {4.1}Framework}{51}{section.4.1}% 
\contentsline {subsection}{\numberline {4.1.1}Note on the effect of $i$ on GDP}{52}{subsection.4.1.1}% 
\contentsline {section}{\numberline {4.2}Looking for Determinacy}{52}{section.4.2}% 
\contentsline {chapter}{\numberline {5}Heterogeneity}{55}{chapter.5}% 
\contentsline {section}{\numberline {5.1}Heterogeneity / Inequality}{55}{section.5.1}% 
\contentsline {subsection}{\numberline {5.1.1}Model}{56}{subsection.5.1.1}% 
\contentsline {subsection}{\numberline {5.1.2}Aggregate Economy}{56}{subsection.5.1.2}% 
\contentsline {subsection}{\numberline {5.1.3}Stationarity (Ergodic Distribution)}{57}{subsection.5.1.3}% 
\contentsline {subsection}{\numberline {5.1.4}Mixing Conditions}{57}{subsection.5.1.4}% 
\contentsline {subsection}{\numberline {5.1.5}Recursive structure}{58}{subsection.5.1.5}% 
\contentsline {section}{\numberline {5.2}Capital Accumulation (Ayagari 1994 QJE)}{63}{section.5.2}% 
\contentsline {subsection}{\numberline {5.2.1}Testing the Model}{63}{subsection.5.2.1}% 
\contentsline {section}{\numberline {5.3}Performance of the Heterogeneous Agent Model}{65}{section.5.3}% 
\contentsline {section}{\numberline {5.4}The RBC Model with Heterogeneous Agents and Incomplete Markets}{66}{section.5.4}% 
\contentsline {section}{\numberline {5.5}Projection and Perturbation Methods}{68}{section.5.5}% 
\contentsline {subsection}{\numberline {5.5.1}Business Cycles:}{70}{subsection.5.5.1}% 
\contentsline {section}{\numberline {5.6}Asset Pricing with Heterogeneous Agents}{70}{section.5.6}% 
\contentsline {subsection}{\numberline {5.6.1}Setup}{71}{subsection.5.6.1}% 
\contentsline {subsection}{\numberline {5.6.2}The Incomplete-Markets Lucas Tree Model}{72}{subsection.5.6.2}% 
\contentsline {subsection}{\numberline {5.6.3}International capital flows}{72}{subsection.5.6.3}% 
\contentsline {subsection}{\numberline {5.6.4}Welfare/Policy}{73}{subsection.5.6.4}% 
\contentsline {section}{\numberline {5.7}Dynamic Contracts}{73}{section.5.7}% 
\contentsline {subsection}{\numberline {5.7.1}1st. friction: Limited Commitment}{73}{subsection.5.7.1}% 
\contentsline {subsection}{\numberline {5.7.2}Decentralization with Limited Commitment}{75}{subsection.5.7.2}% 
\contentsline {section}{\numberline {5.8}Recursive Limited Contracts}{76}{section.5.8}% 
\contentsline {section}{\numberline {5.9}Income Inequality and Consumption Inequality}{79}{section.5.9}% 
\contentsline {subsection}{\numberline {5.9.1}Krueger and Perri (2006)}{79}{subsection.5.9.1}% 
\contentsline {section}{\numberline {5.10}Asymmetric Information}{81}{section.5.10}% 
\contentsline {subsection}{\numberline {5.10.1}Perturbation argument/Proof}{82}{subsection.5.10.1}% 
\contentsline {subsection}{\numberline {5.10.2}Immiseration}{83}{subsection.5.10.2}% 
\contentsline {subsection}{\numberline {5.10.3}Implementation with Taxes}{83}{subsection.5.10.3}% 
