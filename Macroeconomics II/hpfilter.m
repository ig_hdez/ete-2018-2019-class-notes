function hp=hpfilter(y,lambda)
%
% hp=hpfilter(y,lambda)
%
% Applies HP-filter to time series contained in y. y is a matrix which is assumed to be of
%   T rows (time dimension)
%   N columns (each column contains a particular time series)
%
% Returns:
%   hp.mat: the matrix representation of the filter
%   hp.trend: the trend component of the time series
%   hp.cycle: the cyclical component of the time series
%
long    = size(y,1);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                             %
%                Filtre de Hodrick - Prescott                 %
%                                                             %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% function HP=hpfilter(long,lambda);
%
HP=[1+lambda -2*lambda lambda zeros(1,long-3);...
    -2*lambda 1+5*lambda -4*lambda lambda zeros(1,long-4);...
    zeros(long-4,long);...
    zeros(1,long-4) lambda -4*lambda 1+5*lambda -2*lambda;...
    zeros(1,long-3) lambda -2*lambda 1+lambda];

for i=3:long-2;
    HP(i,i-2)=lambda;
    HP(i,i-1)=-4*lambda;
    HP(i,i)=1+6*lambda;
    HP(i,i+1)=-4*lambda;
    HP(i,i+2)=lambda;
end;
hp.mat      = HP;
hp.trend    = HP\y;
hp.cycle    = y-HP\y;