function estim=varest(X,lagx,cst,Z,lagz);

[Tx,nx]     = size(X);
constant    = 0;
exog        = 0;
if nargin>2
    if ~isempty(cst)
        if cst==1;
            constant    = 1;
            Cst         = ones(Tx,1);
        else
            Cst         = [];
        end
    end
end

if nargin>3;      
    exog    = 1;
    [Tz,nz] = size(Z);
    if ~(Tz==Tx);
        error('# of observations should be the same in X and Z');
    else
        T       = Tx;
    end
    if nargin>4;
        nret    = max(lagx,lagz);
    else
        nret    = lagx;
        lagz    = 0;
    end
else
    nz      = 0;     
    nret    = lagx;
    T       = Tx;
end

Xlag    = zeros(T-nret,nx*lagx);
for i=1:lagx;
    Xlag(:,(i-1)*nx+1:i*nx)=X(nret+1-i:T-i,:);
end

if nargin>3
    if nargin>4;
        Zlag    = zeros(T-nret,nz*(1+lagz));
        for i=0:lagz;
            Zlag(:,i*nz+1:(i+1)*nz)=Z(nret+1-i:T-i,:);
        end
    else
        lagz    = 0;
        Zlag    = Z(nret+1:T,:);
    end
else
    lagz    = 0;
    Zlag    = [];
end

if constant
    XX      = [Xlag,Cst(1:T-nret),Zlag];
else
    XX      = [Xlag,Zlag];
end
k       = size(XX,2);
XPX     = XX'*XX;
S       = inv(chol(XPX)');
XPXI    = S'*S;
dfr     = size(XX,2);
YY      = X(nret+1:T,:);
BT      = XX\YY;
UT      = YY-XX*BT;
BT      = BT';
SSR     = UT'*UT;
%SIGMA   = SSR/(T-k);
SIGMA   = SSR/T;
STDBT   = (diag(SIGMA)*diag(XPXI)').^0.5;
TSTAT   = BT./STDBT;
YM      = YY - repmat(mean(YY),T-nret,1);

rsqr1   = diag(SSR);
rsqr2   = diag(YM'*YM);
R2      = 1.0 - rsqr1./rsqr2; % r-squared
rsqr1   = rsqr1/(T-k);
rsqr2   = rsqr2/(T-1.0);
if rsqr2 ~= 0
    RB2 = 1 - (rsqr1./rsqr2); % rbar-squared
else
    RB2 = R2;
end;

% nlag    = floor(T/6);
% bpmod  = sum(autocorr(UT,nlag).^2)*T;
% pvbpmod= 1-chi2cdf(bpmod,nlag);
% lbmod  = sum(autocorr(UT,nlag).^2./(T-(1:nlag)'))*T*(T+2);
% pvlbmod= 1-chi2cdf(lbmod,nlag);

estim.constant = constant;
estim.lagx  = lagx;
estim.lagz  = lagz;
estim.beta  = BT;
estim.tstat = TSTAT;
% estim.pvalt = tdis_prb(TSTAT,T-k);
estim.resid = UT;
estim.sigma = SIGMA;
estim.r2    = R2;
estim.rbar  = RB2;
estim.dyn   = BT(:,1:nx*lagx);
if constant
    estim.c     = BT(:,nx*lagx+1);
end
if exog
    estim.exo   = BT(:,nx*lagx+2:end);
end
estim.stdu  = sqrt(diag(SIGMA));
estim.nvar  = nx;
estim.nexo  = nz+constant;
estim.nobs  = T;
estim.df    = T-k;
estim.mx    = [estim.dyn;eye(nx*(lagx-1)) zeros(nx*(lagx-1),nx)];
if exog
    if constant
        estim.mz    = [estim.c estim.exo;zeros(nx*(lagx-1),k-nx*lagx)];
    else
        estim.mz    = [estim.exo;zeros(nx*(lagx-1),k-nx*lagx)];
    end
end
ldet        = log(det(SIGMA));
nbc         = numel(BT);
estim.aic   = ldet+2*nbc/T;
estim.bic   = ldet+nbc*log(T)/T;
estim.hq    = ldet+2*nbc*log(log(T))/T;
