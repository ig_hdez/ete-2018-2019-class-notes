function irf=comp_irf(sol,ie,nrep,varargin)

nx      = size(sol.mx,1);
S       = zeros(nx,nrep);
init    = zeros(size(sol.me,2),1);

if nargin>3              % to get the IRF to a state rather than a shock
    S(:,1)  = varargin{1};
else
    init(ie)= 1;
    S(:,1)  = sol.me*init;
end
tmp     = S(:,1);
for t=2:nrep
    tmp     = sol.mx*tmp;
    S(:,t)  = tmp;
end
Y=sol.my*S;

irf.x=S';
irf.y=Y';
