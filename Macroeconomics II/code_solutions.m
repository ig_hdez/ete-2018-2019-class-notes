%%%Code for solution of TD2 M2-ETE 2018-2019
% Miguel Zerecero


% Housekeeping
clear
close all
clc

% Import data
DT=importdata('data_quart.csv');

dt = DT.data;
names = DT.textdata(1,2:end);
% Order of variables: real gdp, gdp deflator, Price level (% change of GDP defl),
% M1, Labor share (non-farm business), Fed Funds rate (quarterly average), unemployment rate (quarterly average) 
dates = DT.textdata(2:end,1);
% Dates from 1960Q1 to 2007Q4 (position 80 is 1979q4)

%Define variables
gdp = dt(:,1);
P = dt(:,2);
pi = dt(:,3);
M1 = dt(:,4);
ls = dt(:,5);
R = dt(:,6);
u = dt(:,7);

%% Question 1

%Obtain cycles using HP filter
lambda = 1600;
D = [log(gdp) u ls];
D_c = zeros(length(gdp),3);

for i=1:3
    hp = hpfilter(D(:,i),lambda);
    D_c(:,i) = hp.cycle;
end

%Get correlations

k = 10; %Number of periods to compute forward and backwards correlations
mov_corr = zeros(2*k+1,3);

for i=1:3
    mov_corr(:,i)=f_b_autocorr(D_c(:,i),pi,k);
end

xk = (-k:k);
figure(1)
plot(xk,mov_corr)
axis([-k k -inf inf])
xlabel('k');
legend('GDP','Unemployment','Labor Share');
title('Correlation of x(t) vs Inflation(t+k)')

%% Question 2

%Define cycles of variables
gdp_c = D_c(:,1);
E = [log(P) log(M1)];
E_c = zeros(length(gdp),2);
for i=1:2
    hp = hpfilter(E(:,i),lambda);
    E_c(:,i) = hp.cycle;
end

P_c  = E_c(:,1);
M1_c = E_c(:,2);

figure(2)
plot(1960:0.25:2007.75,[gdp_c M1_c])
legend('GDP','M1');

corr(gdp_c(1:80),M1_c(1:80))
corr(gdp_c(81:end),M1_c(81:end))

figure(3)
plot(1960:0.25:2007.75,[P_c M1_c])
legend('Price level','M1');

corr(P_c(1:80),M1_c(1:80))
corr(P_c(81:end),M1_c(81:end))

%Define matrix of variables
YT = [log(gdp) log(P) R log(M1)];

YB =YT(1:80,:);
YL= YT(81:end,:);

nlags = 2;
nrep    = 20;

[TB,nvar]= size(YB);

%Estimate reduced form VAR
res = varest(YB,nlags);

%Compute IRFs for Monetary Policy shock via the Fed funds rate
shock   = 3;
S       = chol(res.sigma)';
I       = eye(nvar);
mod.mx  = res.mx;
mod.me  = [S;zeros(nvar*(nlags-1),nvar)];
mod.my  = [I zeros(nvar,nvar*(nlags-1))];
irf     = comp_irf(mod,shock,nrep);
irf_R_B = irf.y;
shock   = 4;
irf     = comp_irf(mod,shock,nrep);
irf_M1_B = irf.y; 


[TL,nvar]= size(YL);

%Estimate reduced form VAR
res = varest(YL,nlags);

%Compute IRFs for Monetary Policy shock via the Fed funds rate
shock   = 3;
S       = chol(res.sigma)';
I       = eye(nvar);
mod.mx  = res.mx;
mod.me  = [S;zeros(nvar*(nlags-1),nvar)];
mod.my  = [I zeros(nvar,nvar*(nlags-1))];
irf     = comp_irf(mod,shock,nrep);
irf_R_L = irf.y;
shock   = 4;
irf     = comp_irf(mod,shock,nrep);
irf_M1_L = irf.y; 

figure(4)
subplot(2,2,1);
plot([irf_R_B(:,1) irf_R_L(:,1)])
legend('Before Volcker','After Volcker');
title('GDP')

subplot(2,2,2);
plot([irf_R_B(:,2) irf_R_L(:,2)])
legend('Before Volcker','After Volcker');
title('Price level')

subplot(2,2,3);
plot([irf_R_B(:,3) irf_R_L(:,3)])
legend('Before Volcker','After Volcker');
title('Interest Rate')

subplot(2,2,4);
plot([irf_R_B(:,4) irf_R_L(:,4)])
legend('Before Volcker','After Volcker');
title('M1')

sgtitle('Shock to interest rate')

figure(5)
subplot(2,2,1);
plot([irf_M1_B(:,1) irf_M1_L(:,1)])
legend('Before Volcker','After Volcker');
title('GDP')

subplot(2,2,2);
plot([irf_M1_B(:,2) irf_M1_L(:,2)])
legend('Before Volcker','After Volcker');
title('Price level')

subplot(2,2,3);
plot([irf_M1_B(:,3) irf_M1_L(:,3)])
legend('Before Volcker','After Volcker');
title('Interest Rate')

subplot(2,2,4);
plot([irf_M1_B(:,4) irf_M1_L(:,4)])
legend('Before Volcker','After Volcker');
title('M1')

sgtitle('Shock to M1')

